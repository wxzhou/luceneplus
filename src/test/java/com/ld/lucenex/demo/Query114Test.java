package com.ld.lucenex.demo;

import com.alibaba.fastjson.JSON;
import com.ld.lucenex.base.BaseConfig;
import com.ld.lucenex.base.Const;
import com.ld.lucenex.base.Page;
import com.ld.lucenex.config.LuceneXConfig;
import com.ld.lucenex.core.LuceneX;
import com.ld.lucenex.service.ServiceFactory;
import com.ld.lucenex.service.ServiceImpl;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.nutz.dao.Dao;
import org.nutz.dao.entity.Record;
import org.nutz.dao.impl.NutDao;
import org.nutz.dao.impl.SimpleDataSource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Query114Test {

    @BeforeAll
    public static void start() {
//        Const.FSD_TYPE = "nio";
        //推荐方式
        new LuceneX(new LuceneXConfig() {
            @Override
            public void configLuceneX(BaseConfig me) {
                me.add("d:/test/", "query114", Query114.class);
            }
        });
        GetDao();
    }

    /**
     * 测试 10条数据是否成功
     *
     * @throws IOException
     */
    @Test
    public void testSave() throws IOException {
        List<Query114> empties = new ArrayList<>(10);
        List<Record> rds = dao.query("ln_cust_org", null, null, "cust_full_name,address,id");

        for (Record rd : rds) {

            Query114 empty = new Query114();
            empty.setId(rd.getInt("id"));
            empty.setName(rd.getString("cust_full_name"));
            empty.setAddress(rd.getString("address"));
            empties.add(empty);
        }
        ServiceImpl<Query114> basisService = ServiceFactory.getService(ServiceImpl.class);
        basisService.addObjects(empties);

    }

    /**
     * FuzzyQuery - 模糊查询
     *
     * @throws IOException
     */
    @Test
    public void searchListFuzzyQuery() throws IOException {
        ServiceImpl service = ServiceFactory.getService(ServiceImpl.class, "query114");

        FuzzyQuery fuzzyQuery = new FuzzyQuery(new Term("name", "万达"));
        List<Query114> docs = service.searchList(fuzzyQuery, 10);
        docs.forEach(e -> {
            System.out.println(e);
        });
    }

    private static Dao dao;

    public static Dao GetDao() {
        if (dao != null) return dao;
        SimpleDataSource dataSource = new SimpleDataSource();
        dataSource.setJdbcUrl("jdbc:mysql://127.0.0.1/dev4_liaoning");
        dataSource.setUsername("root");
        dataSource.setPassword("xjjr123");

// 创建一个NutDao实例,在真实项目中, NutDao通常由ioc托管, 使用注入的方式获得.
        dao = new NutDao(dataSource);
        return dao;
    }

    @Test
    public void Search() {
        try {
            IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get("d:/test/test2")));
            IndexSearcher searcher = new IndexSearcher(reader);
            Analyzer analyzer = new StandardAnalyzer();
            QueryParser parser = new QueryParser("title", analyzer);
            Query query = parser.parse("影城万达");
            TopDocs results = searcher.search(query, 100);
            ScoreDoc[] hits = results.scoreDocs;
            for (ScoreDoc s : hits) {
                Document doc = searcher.doc(s.doc);
                System.out.print(doc.get("title"));
                System.out.println(" , " + doc.get("id"));
            }
            System.out.println(hits.length);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void IndexFile() {
        try {
            Directory dir = FSDirectory.open(Paths.get("d:/test/test2"));
            Analyzer analyzer = new StandardAnalyzer();
            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);

            if (true) {
                // Create a new index in the directory, removing any
                // previously indexed documents:
                iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
            } else {
                // Add new documents to an existing index:
                iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
            }
            IndexWriter writer = new IndexWriter(dir, iwc);

            List<Record> rds = dao.query("ln_cust_org", null, null, "cust_full_name,address,id");

            for (Record rd : rds) {
                Document doc = new Document();
                doc.add(new StringField("id", rd.getString("id"), Field.Store.YES));
                doc.add(new TextField("title", rd.getString("cust_full_name"), Field.Store.YES));
                doc.add(new TextField("address", rd.getString("address"), Field.Store.YES));

                if (writer.getConfig().getOpenMode() == IndexWriterConfig.OpenMode.CREATE) {
                    writer.addDocument(doc);
                } else {
                    //writer.updateDocument(new Term("path", ""), doc);
                }

            }
            writer.close();

        } catch (Exception e) {

        }
    }


}
