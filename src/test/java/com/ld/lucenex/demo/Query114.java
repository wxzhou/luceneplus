package com.ld.lucenex.demo;

import com.ld.lucenex.field.FieldKey;
import com.ld.lucenex.field.LDType;

public class Query114 {
    @FieldKey(type = LDType.IntPoint)
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPinyin() {
        return pinyin;
    }

    public void setPinyin(String pinyin) {
        this.pinyin = pinyin;
    }

    @Override
    public String toString() {
        return "Query114{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", pinyin='" + pinyin + '\'' +
                '}';
    }

    @FieldKey(type = LDType.StringField)
    private String name;
    @FieldKey(type = LDType.StringField)
    private String address;
    @FieldKey(type = LDType.StringField)
    private String pinyin;
}
